#-------------------------------------------------------------------------
#  cscsv.py
#  Processes data from Cardsphere ledger files (downloadable with premium)
#-------------------------------------------------------------------------
#   Usage:
#     Method A:
#       1) Place this file (cscsv.py) in the same folder as your ledger.
#       2) Ensure that the name of the ledger file has the form cardsphere-ledger-[anything].csv.
#          Ledger files downloaded from Cardsphere will already have such a name.
#       3) Run this script, i.e. type python cscsv.py in a command line and hit enter. The script will
#          automatically find the ledger file, and if there are multiples it will process the one that
#          has been most recently modified.
#     Method B:
#       1) Run this script with the path to the ledger file as a command line argument, e.g.:
#            python cscsv.py path/to/ledger/ledger.csv
#-------------------------------------------------------------------------

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import os, datetime, re, glob, sys

def addPackageRecord(packageHeadings, packageData, currentPackage):
    for ph in packageHeadings:
        packageData[ph].append(currentPackage[ph])
    if ('Purchase' == currentPackage['Operation']):
        packageData['Size'][-1] *= -1

#------------  Script starts here  -------------
anchorThreshold = 2 # Dollar amount for considering a card to be a package anchor
if (len(sys.argv) > 1):
    # If a command-line argument is given, use it as the ledger file to process
    infileName = sys.argv[1]
else:
    # Find the newest Cardsphere ledger file in the current directory
    times = []
    ledgerFilenames = []
    for file in glob.glob("cardsphere-ledger-*.csv"):
        ledgerFilenames.append(file)
        times.append(os.path.getmtime(file)) #Get the date of last modification as a timestamp (i.e. a floating-point number)
    sortedLedgers = [x for _,x in sorted(zip(times, ledgerFilenames))]
    infileName = sortedLedgers[0]
    print('Processing the most recently modified Cardsphere ledger: ' + infileName)

try:
    infile = open(infileName, 'r')
except:
    print('Could not open the ledger file.')
    sys.exit()

# The data field headings are on the first row separated by ','
headings = infile.readline().strip().split(',')
#Date,Trade #,Operation,Card Name,Set Name,Finish,Condition,Language,Index Price,Current Index Price,
#    Description,Peer User,Peer User ID,Net Amount,Fee,Balance

vals = [ [] for i in range(len(headings)) ]
try:
    for line in infile:
        lineData = line.strip().strip('"').split('","')
        for index, datum in enumerate(lineData):
            vals[index].append(datum)
except UnicodeDecodeError:
    infile.close()
    infile = open(infileName,'r',encoding='utf-8')
    infile.readline()
    for line in infile:
        lineData = line.strip().strip('"').split('","')
        for index, datum in enumerate(lineData):
            vals[index].append(datum)
except:
    print('Could not read the ledger file.')
    sys.exit()
    
data = dict(zip(headings, vals))
infile.close()
nTransactions = len(vals[0])
print('%d total transactions' % nTransactions)

totalFees = 0
totalMembership = 0
totalIndexPrice = 0
totalCurrentIndexPrice = 0
#------------  Convert data types  -------------
for index in range(nTransactions):
    # Date becomes something useful for matplotlib
    (data['Date'])[index] = (data['Date'])[index].split(' ')[0] #Strip out the time and keep only year-month-day
    # Prices, fees, and balances become floats
    try:
        (data['Net Amount'])[index] = float((data['Net Amount'])[index])
    except ValueError:
        (data['Net Amount'])[index] = 0
    
    try:
        (data['Fee'])[index] = float((data['Fee'])[index])
    except ValueError:
        (data['Fee'])[index] = 0
    totalFees += (data['Fee'])[index]
    
    try:
        (data['Index Price'])[index] = float((data['Index Price'])[index])
    except ValueError:
        (data['Index Price'])[index] = 0
    totalIndexPrice += (data['Index Price'])[index]
    
    try:
        (data['Current Index Price'])[index] = float((data['Current Index Price'])[index])
    except ValueError:
        (data['Current Index Price'])[index] = 0
    totalCurrentIndexPrice += (data['Current Index Price'])[index]
    
    try:
        (data['Balance'])[index] = float((data['Balance'])[index])
    except ValueError:
        (data['Balance'])[index] = 0
    
    # UserNumbers become ints
    try:
        (data['Peer User ID'])[index] = int((data['Peer User ID'])[index])
    except ValueError:
        (data['Peer User ID'])[index] = -1
    
    # Trade Numbers become ints
    try:
        (data['Trade #'])[index] = int((data['Trade #'])[index])
    except ValueError:
        (data['Trade #'])[index] = -1

# Rearrange all arrays such that increasing index corresponds to increasing time
for h in headings:
    data[h].reverse()

#---------------  Actual processing of the data  ----------------
nSentCards = 0
nReceivedCards = 0
netAmountReceived = 0
netAmountSent = 0
totalBuyin = 0
totalRefunds = 0
totalMembership = 0
for index, price in enumerate(data['Net Amount']):
    if (price < 0 and 'Purchase' == (data['Operation'])[index]):
        nReceivedCards += 1
        netAmountReceived += price
    elif (price > 0 and 'Sale' == (data['Operation'])[index]):
        nSentCards += 1
        netAmountSent += price
    elif ('Buy-in' == data['Operation'][index]):
        totalBuyin += price
    elif ('Refund' == data['Operation'][index]):
        totalRefunds += price
    elif ('Membership' == data['Operation'][index]):
        totalMembership -= price

# Combine card-by-card records into package records
currentUser = -1
currentPackageSize = 0
currentPackagePrice = 0

buildingPackage = False
packageHeadings = list(headings)
packageHeadings.append('Size')
packageVals = [[] for i in range(len(packageHeadings))]
packageData = dict(zip(packageHeadings, packageVals))
accumulatingHeadings = ['Index Price', 'Current Index Price', 'Net Amount', 'Fee']

currentPackage = dict(zip(packageHeadings, [[] for i in range(len(packageHeadings))]))
for index, userNumber in enumerate(data['Peer User ID']):
    currentType = (data['Operation'])[index]
    if ('Sale' == currentType or 'Purchase' == currentType):
        if (not(buildingPackage)):
            buildingPackage = True
            currentPackage['Size'] = 0
            currentUser = userNumber
            for h in headings:
                currentPackage[h] = data[h][index]
        if (buildingPackage):
            if (userNumber == currentUser):
                currentPackage['Size'] += 1
                currentPackage['Balance'] = data['Balance'][index]
                for ah in accumulatingHeadings:
                    currentPackage[ah] += data[ah][index]
            else:
                # Add the current package to the list
                addPackageRecord(packageHeadings, packageData, currentPackage)
                # Start a new package
                currentUser = userNumber
                currentPackage['Size'] = 1
                for h in headings:
                    currentPackage[h] = data[h][index]
    else:
        # A non-trade transaction has been encountered, so if a package was being built then finalize it
        if (buildingPackage):
            addPackageRecord(packageHeadings, packageData, currentPackage)
            buildingPackage = False
# The end of the last package is not marked by a change in user number, so it must be processed separately.
if (buildingPackage):
    addPackageRecord(packageHeadings, packageData, currentPackage)

nSentPackages = 0
nReceivedPackages = 0
for packagePrice in packageData['Net Amount']:
    if (packagePrice < 0):
        nReceivedPackages += 1
    elif (packagePrice > 0):
        nSentPackages += 1

print('%d cards worth $%4.2f sent in %d packages = $%4.2f/card = $%4.2f/package' % (nSentCards, netAmountSent, nSentPackages, netAmountSent/nSentCards, netAmountSent/nSentPackages))
print('%d cards worth $%4.2f received in %d packages = $%4.2f/card = $%4.2f/package' % (nReceivedCards, -netAmountReceived, nReceivedPackages, -netAmountReceived/nReceivedCards, -netAmountReceived/nReceivedPackages))
print('Total fees = $%4.2f' % (totalFees))
print('Net refunds = $%4.2f' % (totalRefunds))
print('Total buy-in = $%4.2f' % (totalBuyin))
print('Total membership payments = $%4.2f' % (totalMembership))
print('Largest sent package = %d cards' % max(packageData['Size']))
print('Largest received package = %d cards' % -min(packageData['Size']))

prices = data['Net Amount']
index_max = max(range(len(prices)), key=prices.__getitem__)
index_min = min(range(len(prices)), key=prices.__getitem__)
print('Most valuable card sent is %s %s at $%4.2f' % (data['Condition'][index_max], data['Card Name'][index_max], data['Net Amount'][index_max]))
print('Most valuable card received is %s %s at $%4.2f' % (data['Condition'][index_max], data['Card Name'][index_min], -data['Net Amount'][index_min]))

#------------------- Graphs  -------------------
# Set up the x-axis variables for time plots
xdates = [datetime.datetime.strptime(d,'%Y-%m-%d').date() for d in data['Date']] #Dates of individual transactions
xpdates = [datetime.datetime.strptime(d,'%Y-%m-%d').date() for d in packageData['Date']] #Dates of packages completed

figureCounter = 1
# Funds vs time
plt.figure(figureCounter)
figureCounter +=  1
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%y'))
plt.gca().xaxis.set_major_locator(mdates.MonthLocator())
plt.plot(xpdates,packageData['Balance'],'o-')
plt.gcf().autofmt_xdate()
plt.xlabel('Date')
plt.ylabel('Funds [$]')
plt.title('Funds vs. Time')

# Prices of sent/received cards vs time
plt.figure(figureCounter)
figureCounter +=  1
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%y'))
plt.gca().xaxis.set_major_locator(mdates.MonthLocator())
plt.plot(xdates,data['Net Amount'],'o')
plt.plot([xpdates[0],xpdates[-1]],[0,0],'k-')
plt.gcf().autofmt_xdate()
plt.xlabel('Date')
plt.ylabel('Net amount [$]')
plt.title('Prices of traded cards vs. Time')

# Package number vs. time
plt.figure(figureCounter)
figureCounter +=  1
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%y'))
plt.gca().xaxis.set_major_locator(mdates.MonthLocator())
plt.plot(xpdates,packageData['Trade #'],'o-')
plt.gcf().autofmt_xdate()
plt.xlabel('Received date')
plt.ylabel('Package number')
plt.title('Package number vs. Completed date')

# Package size vs time
plt.figure(figureCounter)
figureCounter +=  1
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%y'))
plt.gca().xaxis.set_major_locator(mdates.MonthLocator())
plt.plot(xpdates,packageData['Size'],'o')
plt.plot([xpdates[0],xpdates[-1]],[0,0],'k-')
plt.gcf().autofmt_xdate()
plt.xlabel('Date')
plt.ylabel('Package size')
plt.title('Package sizes vs. Time')

# Package price vs time
plt.figure(figureCounter)
figureCounter +=  1
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%y'))
plt.gca().xaxis.set_major_locator(mdates.MonthLocator())
plt.plot(xpdates,packageData['Net Amount'],'o')
plt.plot([xpdates[0],xpdates[-1]],[0,0],'k-')
plt.gcf().autofmt_xdate()
plt.xlabel('Date')
plt.ylabel('Package price [$]')
plt.title('Prices of packages vs. Time')

# Anchor % vs time
anchorPercentages = []
anchorDates = []
for i in range(nTransactions):
    # The recorded index prices are for NM cards, so skip all non-NM transactions
    if (not('NM' == data['Condition'][i])):
        continue
    try:
        percentage = 100*data['Net Amount'][i]/data['Index Price'][i]
    except:
        continue
    if ('Sale' == data['Operation'][i]):
        if (data['Net Amount'][i] > anchorThreshold):
            anchorPercentages.append(percentage)
            anchorDates.append(xdates[i])
plt.figure(figureCounter)
figureCounter += 1
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%y'))
plt.gca().xaxis.set_major_locator(mdates.MonthLocator())
plt.plot(anchorDates, anchorPercentages, 'o-')
plt.gcf().autofmt_xdate()
plt.xlabel('Date')
plt.ylabel('Price % of anchor cards')
plt.title('NM anchor card (>$%4.2f) price percentage vs. time' % anchorThreshold)

# Distribution of anchor %
plt.figure(figureCounter)
figureCounter += 1
binEdges = range(0,120,10)
(nPackageSizes, bins, patches) = plt.hist(anchorPercentages,bins=binEdges)
plt.xlabel('Price percentage [actual/index]')
plt.ylabel('Frequency')
plt.title('NM anchor card price percentages')
plt.gca().set_xticks(binEdges)
for rect, label in zip(patches, nPackageSizes):
    height = rect.get_height()
    plt.gca().text(rect.get_x() + rect.get_width()/2, height + 1, str(int(label)), ha='center', va='bottom')


# Distribution of package sizes
plt.figure(figureCounter)
figureCounter +=  1
binEdges = [-10.5 + k for k in range(22)]
(nPackageSizes, bins, patches) = plt.hist(packageData['Size'],bins=binEdges)
plt.xlabel('Package size [-:received, +:sent]')
plt.ylabel('Frequency')
plt.title('Sizes of sent and received packages')
plt.gca().set_xticks([-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11])
for rect, label in zip(patches, nPackageSizes):
    height = rect.get_height()
    plt.gca().text(rect.get_x() + rect.get_width()/2, height + 1, str(int(label)), ha='center', va='bottom')

# Distribution of package prices
plt.figure(figureCounter)
figureCounter +=  1
binEdges = [-50,-25,-10,-5,-1,0,1,5,10,25,50]
(nPackagePrices, bins, patches) = plt.hist(packageData['Net Amount'],bins = binEdges)
plt.xlabel('Package price [-:received, +:sent]')
plt.ylabel('Frequency')
plt.title('Distribution of package prices')
plt.gca().set_xticks(binEdges)
for rect, label in zip(patches, nPackagePrices):
    height = rect.get_height()
    plt.gca().text(rect.get_x() + rect.get_width()/2, height + 1, str(int(label)), ha='center', va='bottom')

# Distribution of card prices
plt.figure(figureCounter)
figureCounter +=  1
binEdges = [-50,-25,-10,-5,-1,0,1,5,10,25,50]
(nPrices, bins, patches) = plt.hist(data['Net Amount'],bins = binEdges)
plt.xlabel('Card price [-:received, +:sent]')
plt.ylabel('Frequency')
plt.gca().set_xticks(binEdges)
plt.title('Distribution of card prices')
for rect, label in zip(patches, nPrices):
    height = rect.get_height()
    plt.gca().text(rect.get_x() + rect.get_width()/2, height + 1, str(int(label)), ha='center', va='bottom')

# Histogram of sent/received conditions
sentCondition = {'NM':0, 'SP':0, 'MP':0, 'HP':0}
receivedCondition = {'NM':0, 'SP':0, 'MP':0, 'HP':0}
for (index, condition) in enumerate(data['Condition']):
    try:
        if ((data['Net Amount'])[index] < 0):
            receivedCondition[condition] += 1
        else:
            sentCondition[condition] += 1
    except KeyError:
        # This entry does not have a Condition (could be a refund or buy-in)
        continue

pos = [-4,-3,-2,-1,0,1,2,3,4]
frequencies = [0]*len(pos)
labels = ['HP','MP','SP','NM',' ','NM','SP','MP','HP']
for (index, label) in enumerate(labels):
    if (index < 4):
        frequencies[index] = receivedCondition[label]
    elif (index == 4):
        frequencies[index] = 0
    else:
        frequencies[index] = sentCondition[label]
        
plt.figure(figureCounter)
figureCounter += 1
plt.bar(pos, frequencies, 1.0, color='b')
plt.xlabel('Received  |  Sent')
plt.ylabel('Number of cards')
plt.title('Condition of sent and received cards')
plt.gca().set_xticks(pos)
plt.gca().set_xticklabels(labels)
for rect, label in zip(plt.gca().patches, frequencies):
    height = rect.get_height()
    plt.gca().text(rect.get_x() + rect.get_width()/2, height + 1, str(int(label)), ha='center', va='bottom')
print('---------------------------------------------------')

# Chart of sent/received foil/nonfoil
sentFinish = {'Foil':0, 'Nonfoil':0}
receivedFinish = {'Foil':0, 'Nonfoil':0}
for (index, finish) in enumerate(data['Finish']):
    try:
        if ((data['Net Amount'])[index] < 0):
            receivedFinish[finish] += 1
        else:
            sentFinish[finish] += 1
    except KeyError:
        # This entry does not have a rarity (could be a refund or buy-in)
        continue
        
print('Finish of sent cards:')
for k in sentFinish.keys():
    print('%s : %d' % (k, sentFinish[k]))
print('Finish of received cards:')
for k in receivedFinish.keys():
    print('%s : %d' % (k, receivedFinish[k]))
print('---------------------------------------------------')

# Histogram of user numbers
plt.figure(figureCounter)
figureCounter +=  1
plt.hist(packageData['Peer User ID'], 40)
plt.xlabel('User ID of trade partner')
plt.ylabel('Frequency')
plt.title('Users sent to and received from')

plt.show()